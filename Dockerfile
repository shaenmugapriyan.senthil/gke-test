FROM alpine:3.11
RUN apk update && apk add --no-cache docker-cli
FROM nginx:mainline-alpine
RUN  rm /etc/nginx/conf.d/*
ADD hello.conf /etc/nginx/conf.d/
ADD index.html /usr/share/nginx/html/

